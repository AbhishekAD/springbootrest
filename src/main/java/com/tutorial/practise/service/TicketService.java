package com.tutorial.practise.service;

import com.tutorial.practise.dao.TicketDao;
import com.tutorial.practise.entities.Ticket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TicketService {

    @Autowired
    private TicketDao ticketDao;

    public Ticket createTicket(Ticket ticket){
        return ticketDao.save(ticket);
    }

    public Ticket getTicket(Integer ticketId){
        return ticketDao.findOne(ticketId);
    }

    public Iterable<Ticket> getAllTickets(){
        return ticketDao.findAll();
    }

    public void deleteTicket(Integer ticketId){
        ticketDao.delete(ticketId);
    }

    public Ticket updateTicket(Integer ticketId, String mail){
        Ticket ticket = ticketDao.findOne(ticketId);
        ticket.setEmail(mail);
        Ticket updatedTicket = ticketDao.save(ticket);
        return updatedTicket;
    }
}
