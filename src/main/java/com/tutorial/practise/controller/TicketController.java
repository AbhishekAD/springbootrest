package com.tutorial.practise.controller;

import com.tutorial.practise.entities.Ticket;
import com.tutorial.practise.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class TicketController {

    @Autowired
    private TicketService ticketService;

    @PostMapping(value = "/createTicket")
    public Ticket createTicket(@RequestBody Ticket ticket){
        return ticketService.createTicket(ticket);
    }

    @GetMapping(value = "/ticket/{ticketId}")
    public Ticket getTicket(@PathVariable("ticketId") Integer ticketId){
        return ticketService.getTicket(ticketId);
    }

    @GetMapping(value = "/ticket/allTickets")
    public Iterable<Ticket> getAllTickets(){
        return ticketService.getAllTickets();
    }

    @DeleteMapping(value = "/ticket/{ticketId}")
    public void deleteTicket(@PathVariable("ticketId") Integer ticketId){
        ticketService.deleteTicket(ticketId);
    }

    @PutMapping(value = "/ticket/{ticketId}/{mail:.+}")
    public Ticket updateTicket(@PathVariable("ticketId") Integer ticketId, @PathVariable("mail") String mail){
        return ticketService.updateTicket(ticketId, mail);
    }
}
