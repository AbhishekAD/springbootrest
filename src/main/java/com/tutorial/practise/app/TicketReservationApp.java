package com.tutorial.practise.app;

import com.tutorial.practise.entities.Ticket;
import com.tutorial.practise.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.util.Date;

@SpringBootApplication
@ComponentScan({"com.tutorial.practise"})
@EntityScan("com.tutorial.practise.entities")
@EnableJpaRepositories("com.tutorial.practise.dao")
public class TicketReservationApp implements CommandLineRunner {

    @Autowired
    private TicketService ticketService;

    public static void main(String[] args) {
        SpringApplication.run(TicketReservationApp.class, args);
//      TicketService ticketService = context.getBean("ticketService", TicketService.class);

    }

    @Override
    public void run(String... strings) throws Exception {
        Ticket ticket = new Ticket();
        ticket.setPassengerName("Abhishek");
        ticket.setBookingDate(new Date());
        ticket.setSource("Asansol");
        ticket.setDestination("Banglore");
        ticket.setEmail("abhi@gmail.com");

        ticketService.createTicket(ticket);
    }

}
